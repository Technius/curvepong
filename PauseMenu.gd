extends PopupPanel

export (NodePath) var main_menu
export (NodePath) var game_scene

func _ready():
	$VBoxContainer/RestartButton.connect("button_up", self, "pressed_restart_game")
	$VBoxContainer/ResumeButton.connect("button_up", self, "hide")
	self.connect("popup_hide", self, "unpause")
	self.connect("about_to_show", self, "pause")
	self.set_process_input(true)
	
	if main_menu != null:
		$VBoxContainer/ReturnMainMenuButton.visible = true
		$VBoxContainer/ReturnMainMenuButton.connect("button_up", self, "pressed_return_main_menu")

func pause():
	get_tree().paused = true

func unpause():
	self.hide()
	get_tree().paused = false

func _input(event):
	if not self.visible:
		return

	if event.is_action_pressed("pause_game"):
		self.hide()
		get_tree().set_input_as_handled()

func pressed_restart_game():
	if main_menu != null:
		get_node(main_menu).reset_game()
		get_node(main_menu).launch()
	else:
		get_node(game_scene).get_tree().reload_current_scene()
	unpause()

func pressed_return_main_menu():
	unpause()
	get_tree().paused = true
	get_node(main_menu).return_to_main()