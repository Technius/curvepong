extends Node

export (NodePath) var paddle
export (NodePath) var wizard
export (String) var action_suffix = "_r"
export (bool) var is_bot = true

const Ball = preload("res://Ball.gd")

func _ready():
	assert(paddle != null)
	assert(wizard != null)
	
	paddle = get_node(paddle)
	wizard = get_node(wizard)

func handle(delta, ball, handicap):
	if is_bot:
		handle_bot(delta, ball, handicap)
	else:
		handle_player(delta)

func handle_player(delta):
	if Input.is_action_just_pressed("changespell_ice" + action_suffix):
		paddle.enchant_spell(Ball.PROJ_ICEBOLT)
		wizard.run_enchant_anim()
	elif Input.is_action_just_pressed("changespell_shock" + action_suffix):
		paddle.enchant_spell(Ball.PROJ_SHOCK)
		wizard.run_enchant_anim()

	var vy = 0.0
	if Input.is_action_pressed("move_up" + action_suffix):
		vy -= paddle.speed
	if Input.is_action_pressed("move_down" + action_suffix):
		vy += paddle.speed
	paddle.move_and_collide(Vector2(0, vy))

func handle_bot(delta, ball, handicap):
	var vdist_ball = ball.position.y - paddle.position.y
	vdist_ball = (round(randf()) * 2 - 1 if abs(vdist_ball) < 1 else vdist_ball)
	paddle.move_and_collide(Vector2(0,
		clamp_keep_sign(vdist_ball, paddle.speed * handicap)))
	if wizard.get_node("Sprite/AnimationPlayer").current_animation == "idle":
		if paddle.position.distance_squared_to(ball.position) < 200*200:
			paddle.enchant_spell(randi() % 3)
			wizard.run_enchant_anim()

func clamp_keep_sign(val, max_val):
	return sign(val) * min(abs(val), max_val)