extends Node

var ball_name = "Ball"
const Ball = preload("res://Ball.gd")

const PLAYER_LEFT = 0
const PLAYER_RIGHT = 1

export(float) var BOT_HANDICAP = 0.5
export(int) var MAX_HEALTH = 100
export(NodePath) var pause_menu
export(NodePath) var game_end_popup

var playerScores = [0, 0]
var health = [MAX_HEALTH, MAX_HEALTH]

func _ready():
	get_node(ball_name).connect("out_of_bounds", self, "_on_ball_out_of_bounds")
	var a = get_node(ball_name).connect("hit_wizard", self, "_on_wizard_hit")
	$BoundParticleLeft.connect("animation_finished", self, "on_bound_particle_finish", [$BoundParticleLeft])
	$BoundParticleRight.connect("animation_finished", self, "on_bound_particle_finish", [$BoundParticleRight])
	self.set_physics_process(true)
	if pause_menu == null:
		print("Generating pause menu")
		var inst = load("res://PauseMenu.tscn").instance()
		inst.game_scene = get_path()
		add_child(inst)
		pause_menu = inst.get_path()
	if game_end_popup == null:
		print("Generating game end popup")
		var inst = load("res://GameEndPopup.tscn").instance()
		add_child(inst)
		game_end_popup = inst.get_path()

func _input(event):
	if event.is_action_pressed("pause_game"):
		get_tree().set_input_as_handled()
		get_node(pause_menu).popup()

func _physics_process(delta):
	var ball = get_node(ball_name)
	$ControllerRight.handle(delta, ball, BOT_HANDICAP)
	$ControllerLeft.handle(delta, ball, BOT_HANDICAP)

func from_player_id(id, left, right):
	return left if id == PLAYER_LEFT else right

func _on_ball_out_of_bounds(losingPlayerId):
	var ball = get_node(ball_name)
	var oob_anim = from_player_id(losingPlayerId, $BoundParticleLeft, $BoundParticleRight)
	oob_anim.visible = true
	oob_anim.frame = 0
	oob_anim.play()
	oob_anim.position = Vector2(oob_anim.position.x, ball.position.y)
	$OobSound.play()
	
	var playerId = from_player_id(losingPlayerId, PLAYER_RIGHT, PLAYER_LEFT)
	playerScores[playerId] += 1
	update_score_labels(playerId)
	ball.position = Vector2(640, 360)
	ball.rotation = from_player_id(losingPlayerId, 0, PI)
	ball.set_spell_type(Ball.PROJ_FIREBALL)
	update_health(losingPlayerId, health[losingPlayerId] - ball.get_damage() / 4)

func _on_wizard_hit(playerId):
	if health[playerId] == 0:
		# Already dead, so don't process
		return

	var wiz = from_player_id(playerId, get_node("WizardLeft"), get_node("WizardRight"))
	wiz.run_hit_effects()
	update_health(playerId, health[playerId] - get_node(ball_name).get_damage())

func update_health(playerId, value):
	var oldHp = health[playerId]
	var newHp = max(0, value)
	health[playerId] = newHp
	var bar = from_player_id(playerId, get_node("UI/HealthLeft/Bar"), get_node("UI/HealthRight/Bar"))
	var old_region = bar.region_rect
	var new_size = Vector2(newHp / float(MAX_HEALTH) * bar.texture.get_width(), old_region.size.y)
	bar.region_rect = Rect2(old_region.position, new_size)
	
	if oldHp > 0 and newHp == 0:
		self.set_physics_process(false)
		var wiz = from_player_id(playerId, get_node("WizardLeft"), get_node("WizardRight"))
		wiz.run_death_anim()
		var animPlayer = wiz.get_node("Sprite/AnimationPlayer")
		var res = animPlayer.connect("animation_finished", self, "end_game", [animPlayer])

func update_score_labels(playerId):
	var label = from_player_id(playerId, get_node("UI/LeftScore"), get_node("UI/RightScore"))
	if label != null:
		label.text = str(playerScores[playerId])

func end_game(ignored, eventSource):
	eventSource.disconnect("animation_finished", self, "end_game")
	get_tree().paused = true
	get_node(game_end_popup).show()

func on_bound_particle_finish(sprite):
	sprite.visible = false