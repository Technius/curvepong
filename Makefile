all: linux web

linux:
	rm -rf dist/linux
	mkdir -p dist/linux
	godot --export "linux" dist/linux/spellball

web:
	rm -rf dist/web
	mkdir -p dist/web
	godot --export "web" dist/web/spellball.html

.PHONY: all linux web
