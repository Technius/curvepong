extends KinematicBody2D

signal out_of_bounds
signal hit_wizard

export (Array, int) var projSpeeds
export (Array, NodePath) var impactSounds
export (Array, int) var projDamage
export (int) var projectileType = 0

const PROJ_FIREBALL = 0
const PROJ_ICEBOLT = 1
const PROJ_SHOCK = 2

func _ready():
	self.set_physics_process(true)

func _physics_process(delta):
	var rem = delta * get_speed() * Vector2(cos(self.rotation), sin(self.rotation))
	var collide_info = self.move_and_collide(rem)
	var max_iter = 4
	var iter_count = 0
	while collide_info != null and iter_count < max_iter:
		var surface_norm = collide_info.normal
		if collide_info.collider.is_in_group("paddle"):
			var paddle = collide_info.collider
			set_spell_type(paddle.enchanted_spell)
			
			if paddle.enchanted_spell == PROJ_SHOCK:
				# Add random variation between (-0.25, 0.25) to vertical direction
				var y_dev = 0.25 - randf() * 0.5
				var dir = Vector2(surface_norm.x, y_dev).normalized()
				rem = collide_info.remainder.length() * dir
			else:
				var target_dir = (collide_info.position - paddle.position).normalized()
				var ang = surface_norm.angle_to(target_dir)
				rem = collide_info.remainder.length() * target_dir.rotated(-ang / 2)
			
			paddle.enchant_spell(paddle.default_spell)
		else:
			rem = -collide_info.remainder.reflect(surface_norm)
		
		if collide_info.collider.is_in_group("boundary"):
			self.emit_signal("out_of_bounds", collide_info.collider.playerId)
			return
		elif collide_info.collider.is_in_group("wizard"):
			self.emit_signal("hit_wizard", collide_info.collider.playerId)
		
		self.rotation = rem.angle()
		# Prevent near-vertical bouncing by adjusting angle very slightly
		if (abs(abs(self.rotation) - PI / 2) < PI / 24):
			self.rotation = self.rotation + PI / 24
		
		collide_info = self.move_and_collide(rem)
		iter_count += 1

	if iter_count > 0:
		get_node(impactSounds[self.projectileType]).play()

func get_speed():
	return projSpeeds[projectileType]

func set_spell_type(ty):
	if ty == self.projectileType:
		return

	var texturePath
	if ty == PROJ_FIREBALL:
		texturePath = "res://assets/fireball-spell.png"
	elif ty == PROJ_ICEBOLT:
		texturePath = "res://assets/icicle-spell.png"
	elif ty == PROJ_SHOCK:
		texturePath = "res://assets/shock-spell.png"
	else:
		return
	
	$Sprite.texture = load(texturePath)
	self.projectileType = ty

func get_damage():
	return projDamage[projectileType]