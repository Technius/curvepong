extends KinematicBody2D

export (float) var speed = 10.0
export (int) var player_id = -1
export (int, 0, 2) var default_spell = 0
export (Array, Color) var spell_colors

const default_spell_colors = [
	Color("fce708"),
	Color("93fffb"),
	Color("3a55ea")
]

const Ball = preload("res://Ball.gd")

var enchanted_spell = Ball.PROJ_FIREBALL
var mat = preload("res://PaddleMaterial.tres")

func _ready():
	assert(player_id != -1)
	if spell_colors == null:
		spell_colors = default_spell_colors
	$Sprite.material = mat.duplicate()
	enchant_spell(self.enchanted_spell)
	$Sprite.play()

func enchant_spell(proj):
	self.enchanted_spell = proj
	$Sprite.material.set_shader_param("target_color", spell_colors[proj])
	