extends KinematicBody2D

export(int) var playerId

func _ready():
	pass

func run_hit_effects():
	$GruntSound.play()
	$ShieldEffect/AnimationPlayer.play("on_hit")
	# $Sprite/AnimationPlayer.connect("animation_finished", self, "on_enchant_anim_finish")

func run_death_anim():
	$Sprite/AnimationPlayer.play("death")

func run_enchant_anim():
	$Sprite/AnimationPlayer.play("cast")
	$Sprite/AnimationPlayer.queue("idle")