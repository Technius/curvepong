#!/bin/bash
export ASSETS_DIR=assets
export DOWNLOAD_DIR=assets_download
mkdir -p $ASSETS_DIR $DOWNLOAD_DIR

download_archive() {
    if [ ! -f $DOWNLOAD_DIR/$2 ]; then
        echo "Downloading $1 and saving to $DOWNLOAD_DIR/$2"
        curl -L $1 -o $DOWNLOAD_DIR/$2
        mkdir -p $DOWNLOAD_DIR/$3
        unzip $DOWNLOAD_DIR/$2 -d $DOWNLOAD_DIR/$3
    fi
}

download_file () {
    if [ ! -f $ASSETS_DIR/$2 ]; then
        echo "Downloading $1 and saving to $ASSETS_DIR/$2"
        curl -L $1 -o $ASSETS_DIR/$2
    fi
}

unzip_file () {
    mkdir -p "$DOWNLOAD_DIR/$2"
    unzip "$DOWNLOAD_DIR/$1" -d "$DOWNLOAD_DIR/$2"
}

copy_ar_file() {
    mkdir -p "$ASSETS_DIR/$1"
    cp "$DOWNLOAD_DIR/$1/$2" "$ASSETS_DIR/$1/${3-$2}"
}

download_archive https://opengameart.org/sites/default/files/spells.zip spells.zip spells

download_file 'https://opengameart.org/sites/default/files/wizard%20spritesheet%20calciumtrice.png' wizard.png

download_archive https://opengameart.org/sites/default/files/Battle-background-hazy-hills-files_1.zip hazy-hills.zip hazy-hills
copy_ar_file hazy-hills Battle-background-hazy-hills-files/PNG/battle-background-sunny-hillsx4.png

download_file 'https://opengameart.org/sites/default/files/gruntsound.wav' grunt.wav

download_file https://opengameart.org/sites/default/files/fireball_0.png fireball-spell.png
download_file https://opengameart.org/sites/default/files/icicle_0.png icicle-spell.png
download_file https://opengameart.org/sites/default/files/shock.png shock-spell.png

mkdir -p $ASSETS_DIR/flare_hud
download_file https://opengameart.org/sites/default/files/bar_hp_mp.png flare_hud/hp_mp_frame.png
download_file https://opengameart.org/sites/default/files/bar_hp.png flare_hud/hp_bar.png

download_file https://opengameart.org/sites/default/files/spr_shield.png shield.png

# spells.zip actually contains .wav files, not .ogg files
copy_ar_file spells zap13.ogg zap13.wav
copy_ar_file spells zap8a.ogg zap8a.wav
# for f in $DOWNLOAD_DIR/spells/*.ogg; do
#     mv "$f" "$ASSETS_DIR/spells/$(basename ${f/\.ogg/\.wav})"
# done

download_archive 'https://opengameart.org/sites/default/files/Fall%20of%20Arcana%20%28NEV%29.zip' fall_of_arcana.zip fall_of_arcana
if [ ! -f $ASSETS_DIR/fall_of_arcana_looped.ogg ]; then
    ffmpeg -i "$DOWNLOAD_DIR/fall_of_arcana/Fall of Arcana (NEV)/Fall of Arcana (New Era Version) Looped.wav" $ASSETS_DIR/fall_of_arcana_looped.ogg
fi

download_archive 'https://opengameart.org/sites/default/files/sci-fi-effects.zip' sci-fi-effects.zip sci-fi-effects
mkdir -p $ASSETS_DIR/sci-fi-effects/pulsating_shield
for i in $(seq 0 7); do
    copy_ar_file sci-fi-effects pulsating_shield/0$i.png
done
mkdir -p $ASSETS_DIR/sci-fi-effects/waves
for i in $(seq 0 6); do
    copy_ar_file sci-fi-effects waves/0$i.png
done
