extends PopupPanel

export (NodePath) var main_menu

func _ready():
	$VBoxContainer/RestartButton.connect("button_up", self, "on_restart_game")
	if main_menu != null:
		main_menu = get_node(main_menu)
		var mm_btn = $VBoxContainer/MainMenuButton
		mm_btn.connect("button_up", self, "on_return_main")
		mm_btn.visible = true

func on_restart_game():
	if main_menu == null:
		get_tree().paused = false
		get_tree().reload_current_scene()
	else:
		self.hide()
		main_menu.reset_game()
		main_menu.launch()

func on_return_main():
	self.hide()
	main_menu.return_to_main()