extends Node

var Main = preload("res://Main.tscn")
var game_instance
var is_pvp = false

func _ready():
	print("Game started")
	get_tree().paused = true
	var buttons = $Overlay/MenuPanel/VBoxContainer
	buttons.get_node("PvpButton").connect("button_up", self, "launch_pvp")
	buttons.get_node("PvbButton").connect("button_up", self, "launch_pvb")
	buttons.get_node("CreditsButton").connect("button_down", $Overlay/MenuPanel/CreditsDialog, "show")
	game_instance = $Main

func launch_pvp():
	self.is_pvp = true
	launch()

func launch_pvb():
	self.is_pvp = false
	launch()

func launch():
	game_instance.get_node("ControllerLeft").is_bot = not self.is_pvp
	get_tree().paused = false
	$Overlay/MenuPanel.visible = false

func reset_game():
	var oldMain = game_instance
	self.remove_child(oldMain)
	oldMain.queue_free()

	var newMain = Main.instance()
	newMain.pause_menu = $Overlay/PauseMenu.get_path()
	newMain.game_end_popup = $Overlay/GameEndPopup.get_path()
	self.add_child(newMain)
	$Overlay/PauseMenu.game_scene = newMain.get_path()
	game_instance = newMain

func return_to_main():
	reset_game()
	$Overlay/MenuPanel.visible = true